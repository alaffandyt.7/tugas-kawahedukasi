// Taopik Romdoni
package tugas1;
import java.io.FileWriter;
import java.util.*;

public class TugasPekan1 {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int angka1 = 0, angka2 = 0, menu = 0, hasil = 0;
        double hasilBagi = 0d;
        char ulangMenu;

        // menggunkan do while agar program dapat terus berjalan
        do
        {
            System.out.println("====Kalkulator====");
            System.out.println("..................");
            System.out.println("Menu :");
            System.out.println("1. Penjumlahan");
            System.out.println("2. Pengurangan");
            System.out.println("3. Perkalian");
            System.out.println("4. Permbagian");
            System.out.println("5. Sisa bagi");
            System.out.println();

            /*  menggunakan try catch agar ketika user menginput 
            pilihan menu yang tidak sesuai, maka otomatis akan memilih menu 0*/
            try{
                System.out.print("Masukan nomor menu operasi perhitungan : ");
                menu = userInput.nextInt();
            } catch (Exception e) {
                String dump = userInput.nextLine();
                menu = 0;
            }

            //menggunakan if else agar ketika menu = 0, bagian ini akan dilewati
            if (menu != 0) {
                System.out.print("Masukan angka : ");
                angka1 = userInput.nextInt();
                System.out.print("Masukan angka : ");
                angka2 = userInput.nextInt();
            }

            // Switch untuk menjalankan program sesuai dengan pilihan menu 
            switch (menu) {
                case 1:
                    hasil = angka1 + angka2;
                    System.out.println(angka1 +" + "+angka2+" = " + hasil);
                    break;
                case 2:
                    hasil = angka1 - angka2;
                    System.out.println(angka1 +" - "+angka2+" = " + hasil);
                    break;
                case 3:
                    hasil = angka1 * angka2;
                    System.out.println(angka1 +" * "+angka2+" = " + hasil);
                    break;
                case 4:
                //untuk operasi pembagian, variabel perlu dikonversi kedalam tipe data double/float
                    double angkaD1 = angka1;
                    double angkaD2 = angka2;
                    hasilBagi = angkaD1 / angkaD2;
                    System.out.println(angka1 +" : "+angka2+" = " + hasilBagi);
                    break;
                case 5:
                    hasil = angka1 % angka2;
                    System.out.println(angka1 +" % "+angka2+" = " + hasil);
                    break;
                case 0:
                    System.out.println("Menu yang anda masukan belum tersedia.");
                default:
                    break;
            }
            // program untuk menyimpan hasil perhitungan
            try{
                FileWriter saveHasil = new FileWriter("result.txt");
                /*menggunakan if else, karena hasil pembagian disimpan pada variabel yang berbeda
                 *sehingga program hanya akan menyimpan hasil dari perhitungan yang dijalankan
                */
                if (hasil == 0) {
                    saveHasil.write("Hasil Perhitungan adalah :" + Double.toString(hasilBagi));
                } else {
                    saveHasil.write("Hasil perhitungan adalah : " + Integer.toString(hasil));
                }
                saveHasil.close();
                System.out.println("Hasil perhitungan berhasil disimpan.");
            } catch (Exception e){
                System.out.println("Terjadi kesalahan.");
                e.printStackTrace();
            }
            // program akan berhenti hanya ketika user menginput huruf t
            System.out.println("Apakah anda ingin memilih menu lain ? [y/t]");
            ulangMenu = userInput.next().charAt(0);

        } while (ulangMenu != 't');

        System.out.println("Aplikasi kalkulator berakhir, terima kasih.");
    }
}