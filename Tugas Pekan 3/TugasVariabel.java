package tugaspekan3;

import java.util.Scanner;

public class TugasVariabel {
    public static void main(String[] args) {

        // Soal 1. a
        String nama;
        Scanner userInput = new Scanner(System.in);
        System.out.print("Masukan nama : ");
        nama = userInput.nextLine();  
        System.out.println("Selamat datang di Boothcamp Kawah Edukasi," + nama);  

        //Soal 1.b
        short shortValue = 1;
        short shortValue2 = 2;
        double doubleValue = 2.0d ;
        double doubleValue2 = 3.0d;
        char charVariable = 'a';
        char charVariable2 = 'b';
        boolean booleanVariable = true;
        boolean booleanVariable2 = false;

        //Soal 1.c
        String value1 = "Saya senang ";
        String value2 = "belajar ";
        String value3 = "Java Language.";
        
        System.out.println(value1 + value2 + value3);
    }

}
