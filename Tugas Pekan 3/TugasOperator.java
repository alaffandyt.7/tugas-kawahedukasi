package tugaspekan3;

import java.util.Scanner;

public class BangunDatar {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int menu;
        double luasLingkaran, volumeBalok;
        float phi = 3.14f;
        float jariJari;
        float panjang, lebar, tinggi;
        System.out.println("==== Menghitung Bangun Ruang ====");
        System.out.println("...............................");
        System.out.println("Menu :");
        System.out.println("1. Luas Lingkaran");
        System.out.println("2. Volume Balok");
        System.out.println();

        System.out.print("Masukan pilihan menu : ");
        menu = userInput.nextInt();

        switch (menu) {
            case 1 :
                System.out.println("#PROGRAM MENGHITUNG LUAS LINGKARAN#");
                System.out.println("-----------------------------------");
                System.out.println();
                System.out.print("Maukan jari-jari : ");
                jariJari = userInput.nextFloat();
                luasLingkaran = phi * jariJari * jariJari;
                System.out.println("Luas lingkaran = " + luasLingkaran);
                break;
            case 2 :
                System.out.println("#PROGRAM MENGHITUNG LUAS LINGKARAN#");
                System.out.println("-----------------------------------");
                System.out.println();
                System.out.print("Maukan panjang balok : ");
                panjang = userInput.nextFloat();
                System.out.print("Maukan lebar balok : ");
                lebar = userInput.nextFloat();
                System.out.print("Maukan tinggi balok : ");
                tinggi = userInput.nextFloat();
                volumeBalok = panjang * lebar * tinggi;
                System.out.print("Luas lingkaran = " + volumeBalok + " kubik");
                break;
            default:
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Pogram Selesai, Terima kasih.");
    }
    
}
