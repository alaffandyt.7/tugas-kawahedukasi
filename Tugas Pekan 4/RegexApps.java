package tugaspekan4;

import java.util.Scanner;
import java.util.regex.Pattern;

class Cekdata {
    static Pattern date_Pattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
    static Pattern ip_Pattern = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.(?!$)|$)){4}$");
    static Pattern email_Pattern = Pattern.compile("^(.+)@(\\S+)$");


    public static String cekInput (String input) {
        if(date_Pattern.matcher(input).matches()) {
            return "This is Date";
        } else if (ip_Pattern.matcher(input).matches()) {
            return "This is IP";
        } else if (email_Pattern.matcher(input).matches()) {
            return "This is Email";
        } else {
            return "Your input is not correct";
        }
    }
}


public class RegexApps {
    public static void main(String[] args) {
        Scanner inputUser = new Scanner(System.in);
        System.out.print("Please enter Date/IP address/E-mail : ");
        String input = inputUser.nextLine();
        System.out.println(Cekdata.cekInput(input));
 
    }
}