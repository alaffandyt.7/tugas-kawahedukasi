package tugaspekan4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ListApp {
    public static void main(String[] args) {
        //List 10 makanan
        List<String> namaMakanan = new ArrayList<>();
        namaMakanan.add("Bakwan");
        namaMakanan.add("Batagor");
        namaMakanan.add("Kupat Tahu");
        namaMakanan.add("Combro");
        namaMakanan.add("Sate");
        namaMakanan.add("Rendang");
        namaMakanan.add("Soto");
        namaMakanan.add("Gepuk");
        namaMakanan.add("Ayam Bakar");
        namaMakanan.add("Karedok");
        System.out.println(namaMakanan);

        //List 3 tahun piala dunia
        List<Integer> tahunPialaDunia = new ArrayList<>();
        tahunPialaDunia.add(2018);
        tahunPialaDunia.add(2014);
        tahunPialaDunia.add(2010);
        System.out.println(tahunPialaDunia);

        //Sort asc list Provinis di indonesia
        List<String> provinsi = new ArrayList<>();
        provinsi.add("Jawa Barat");
        provinsi.add("Jawa Timur");
        provinsi.add("Jawa Tengah");
        provinsi.add("DKI Jakarta");
        provinsi.add("DI Yogyakarta");
        provinsi.add("Aceh");
        provinsi.add("Sumatera Utara");
        provinsi.add("Sumatera Selatan");
        provinsi.add("Kalimantan Timur");
        provinsi.add("Kalimantan Tengah");
        provinsi.sort(Comparator.naturalOrder());
        System.out.println(provinsi);

        //Hapus data berebeda dari list yang bersisi 12 bahasa pemrograman
        List<String> bahasaPemrograman = new ArrayList<>();
        bahasaPemrograman.add("Java");
        bahasaPemrograman.add("Phyton");
        bahasaPemrograman.add("C#");
        bahasaPemrograman.add("C++");
        bahasaPemrograman.add("Kotlin");
        bahasaPemrograman.add("Groovy");
        bahasaPemrograman.add("C");
        bahasaPemrograman.add("PHP");
        bahasaPemrograman.add("Javascript");
        bahasaPemrograman.add("Pascal");
        bahasaPemrograman.add("Assembly");
        bahasaPemrograman.add("Ruby");
     
        for(int i=0; i < 5; i++) {
            bahasaPemrograman.remove(i);
        }
        System.out.println(bahasaPemrograman);
        


        
    }

    
}
